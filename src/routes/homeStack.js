import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'

/* Screens ========== */
import Home from '../screens/Home/index'
import ReviewDetails from '../screens/ReviewDetails/index'

import Header from '../components/Header/index'

const screens = {
  Home: {
    screen: Home,
    navigationOptions: ({ navigation }) => {
      return ({
      header: () => <Header navigation={navigation} />,
    })}
  },
  ReviewDetails: {
    screen: ReviewDetails,
    navigationOptions: {
      title: "Review Details"
    }
  }
}

const HomeStack = createStackNavigator(screens, {
  defaultNavigationOptions: {
    headerTintColor: "#444",
    headerStyle: { backgroundColor: "#EEE"}
  }
})

export default HomeStack;