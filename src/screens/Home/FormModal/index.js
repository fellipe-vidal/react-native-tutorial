import React from 'react'
import { Modal, SafeAreaView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

import Form from '../Form/index'

import styles from '../styles'

const FormModal = ({ visible, closeModal, addReview }) => {
  return (

    <Modal visible={visible} animationType="slide">
      <SafeAreaView style={styles.modalContent}>
        <Icon
          name="close"
          size={24}
          onPress={ () => closeModal() } 
          style={{ ...styles.modalToggle, ...styles.modalClose }}
        />

        <Form addReview={addReview} />
      </SafeAreaView>
    </Modal>

  )
}

export default FormModal


