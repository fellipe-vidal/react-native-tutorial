import { StyleSheet } from 'react-native';

const globalStyles = StyleSheet.create({

  container: {
    flex: 1,
    padding: 20,
    backgroundColor: "#FEFEFE"
  },

  titleText: {
    fontSize: 18,
    color: "#333",
  },

  paragraph: {
    marginVertical: 8,
    lineHeight: 20,
    opacity: 0.7
  },

  input: {
    borderWidth: 1,
    borderColor: "#DDD",
    padding: 10,
    fontSize: 18,
    borderRadius: 6
  },

  errorText: {
    color: "crimson",
    fontWeight: "bold",
    marginBottom: 10,
    marginTop: 6,
  }

});

export const images = {
  ratings: {
    "1": require("../assets/images/rating-1.png"),
    "2": require("../assets/images/rating-2.png"),
    "3": require("../assets/images/rating-3.png"),
    "4": require("../assets/images/rating-4.png"),
    "5": require("../assets/images/rating-5.png"),
  }
}

export default globalStyles;