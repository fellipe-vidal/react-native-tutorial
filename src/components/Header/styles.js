import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({

  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 10
  },

  headerText: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#333",
    letterSpacing: 1,
  },

  icon: {
    position: "absolute",
    left: 16,

  },

  logoContainer: {
    flexDirection: "row",
    alignItems: "center"
  },

  logo: {
    width: 20,
    height: 20,
    marginRight: 15
  }

})

export default styles;