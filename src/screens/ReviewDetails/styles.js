import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

  rating: {
    flexDirection: "row",
    marginTop: 20,
    paddingTop: 20,
    justifyContent: "center",
    borderTopWidth: 1,
    borderTopColor: "#CCC"
  },

  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#444"
  }

});

export default styles;