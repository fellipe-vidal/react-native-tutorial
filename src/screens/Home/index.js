import React, { useState } from 'react'
import { View, Text, FlatList, TouchableOpacity, SafeAreaView } from 'react-native'

import FormModal from './FormModal/index.js'


/* Styles Imports ========== */
import styles from './styles'
import globalStyles from '../../styles/global'
import Card from '../../components/Card/index'
import Icon from 'react-native-vector-icons/MaterialIcons'


/* Home ========== */
const Home = ({ navigation }) => {

  const [isModalOpen, setModalOpen] = useState(false)
  const [reviews, setReviews] = useState([
    { title: "Zelda, Breath Of Fresh Air", rating: 5, body: "Lorem ipsum dolor sit amet", key: "1" },
    { title: "Gotta Catch'em All (Again)", rating: 4, body: "Lorem ipsum dolor sit amet", key: "2" },
    { title: "Not So Final Fantasy", rating: 3, body: "Lorem ipsum dolor sit amet", key: "3" },
  ])

  const addReview = (newReview) => {
    newReview.key = Math.random().toString();
    setReviews((prevReviews) => [ newReview, ...prevReviews ])
    setModalOpen(false)
  }


  return (
    <View style={globalStyles.container}>

      <FormModal visible={isModalOpen} closeModal={ () => setModalOpen(!isModalOpen)} addReview={addReview} />

      <Icon
        name="add"
        size={24}
        onPress={ () => setModalOpen(true) }
        style={styles.modalToggle} 
      />

      <FlatList 
        data={reviews}
        renderItem={ ({item}) => 
          <Card>
            <TouchableOpacity onPress={() => navigation.navigate("ReviewDetails", item)} >
            <Text style={globalStyles.titleText}>{item.title}</Text>
            </TouchableOpacity>
          </Card>
        }
      />
    </View>
  )

}

export default Home
