import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'

/* Screens ========== */
import About from '../screens/About/index'

import Header from '../components/Header/index'

const screens = {
  About: {
    screen: About,
    navigationOptions: ({navigation}) => {
      return ({
      header: () => <Header navigation={navigation} />,
    })}
  }
}

const AboutStack = createStackNavigator(screens, {
  defaultNavigationOptions: {
    headerTintColor: "#444",
    headerStyle: { backgroundColor: "#EEE"}
  }
})

export default AboutStack
