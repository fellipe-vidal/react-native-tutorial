import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'


/* Styles Import ========== */
import styles from './styles'
import globalStyles, { images } from '../../styles/global'
import Card from '../../components/Card/index'


/* ReviewDetails ========== */
const ReviewDetails = ( {navigation} ) => {
  return (
    <View style={globalStyles.container}>
      <Card>
        <Text style={styles.title}>{ navigation.getParam("title") }</Text>
        <Text style={globalStyles.paragraph}>{ navigation.getParam("body") }</Text>
        <View style={styles.rating}>
          <Text>GameZone rating: </Text>
          <Image source={images.ratings[navigation.getParam("rating")]} />
        </View>
      </Card>
      
    </View>
  )
}

export default ReviewDetails
