import React from 'react'
import { View, Text } from 'react-native'


/* Styles Import ========== */
import globalStyles from '../../styles/global';

const About = () => {
  return (
    <View style={globalStyles.container}>
      <Text>About Screen</Text>
    </View>
  )
}

export default About
