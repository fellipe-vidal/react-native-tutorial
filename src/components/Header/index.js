import React from 'react'
import { StyleSheet, Text, View, SafeAreaView, ImageBackground, Image } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import heartLogo from '../../assets/images/heart_logo.png'
import image_background from '../../assets/images/game_bg.png'

// import styles from './styles'

const Header = ( {navigation} ) => {

  const onPressHandler = () => {
    navigation.openDrawer()
  }

  return(

    <ImageBackground resizeMode="cover" source={image_background} style={{flex:1}}>
      <SafeAreaView>
        <View style={styles.header} >
          <Icon name="menu" size={28} onPress={onPressHandler} style={styles.icon} />
          <View style={styles.logoContainer}>
            <Image style={styles.logo} source={heartLogo} />
            <Text style={styles.headerText} >GameZone</Text>
          </View>
        </View>
      </SafeAreaView>
    </ImageBackground>

  )

};

const styles = StyleSheet.create({

  header: {
    flexDirection: "row",
    justifyContent: "center",
    padding: 10,
  },

  headerText: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#333",
    letterSpacing: 1,
  },

  icon: {
    position: "absolute",
    left: 16,

  },

  logoContainer: {
    flexDirection: "row",
    alignItems: "center"
  },

  logo: {
    width: 20,
    height: 20,
    marginRight: 15
  }

})

export default Header;