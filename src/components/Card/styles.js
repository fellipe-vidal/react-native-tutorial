import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  card: {
    margin: 5,
    padding: 20,
    shadowOpacity: 0.15,
    shadowOffset: {height: 2},
    shadowRadius: 3,
    borderRadius: 10,
    backgroundColor: "#FEFEFE"
  }
})

export default styles