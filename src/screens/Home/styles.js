import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

  modalToggle: {
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "#BBB",
    padding: 10,
    borderRadius: 22,
    alignSelf: "center",
  },

  modalClose: {
    marginTop: 20,
    marginBottom: 10
  },

  modalContent: {
    flex: 1,
  }

});

export default styles;